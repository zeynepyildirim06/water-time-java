package com.example.watertime;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.example.watertime.R.style;
import com.xw.repo.BubbleSeekBar;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static androidx.core.content.ContextCompat.getSystemService;


public class Settings_Fragment extends Fragment {

    LinearLayout layout_unit, layout_popup_unit, layout_consumption;
    View view;
    BubbleSeekBar bubbleSeekBar;
    TextView txt_ml;
    int txt_seekbar;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate( R.layout.fragment_settings, container, false );
        layout_unit = view.findViewById( R.id.layout_settings_linearlayout_unit );
        layout_consumption = view.findViewById( R.id.layout_settings_linearlayout_kapasite );
        txt_ml = view.findViewById( R.id.layout_consumption_target_txt );

        layout_unit.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.Instance.setPopUp( v );
            }
        } );

        layout_consumption.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.Instance.setConsumtionPopPup( v );

            }
        } );

        return view;

    }

}
