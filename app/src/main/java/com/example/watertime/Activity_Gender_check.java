package com.example.watertime;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class Activity_Gender_check extends AppCompatActivity {
ImageView imgGirl,imgBoy;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_gender_check );
        imgGirl=findViewById( R.id.activity_gender_check_img_girl);
        imgBoy=findViewById( R.id.activity_gender_check_img_boy);

        imgBoy.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( getApplicationContext(), Activity_User_Weight.class );
                startActivity( intent );
            }
        } );

        imgGirl.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( getApplicationContext(), Activity_User_Weight.class );
                startActivity( intent );
            }
        } );
    }
}
