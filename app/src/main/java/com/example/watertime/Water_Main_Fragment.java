package com.example.watertime;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;


public class Water_Main_Fragment extends Fragment {
View view;
    int pStatus = 0;
    private Handler handler = new Handler();
    TextView tv;
    ProgressBar mProgress;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view=inflater.inflate( R.layout.fragment_water_main, container, false );
        LoadCircularProgressBar();
        return view;
    }
    private void LoadCircularProgressBar() {
        Resources res = getResources();
        Drawable drawable = res.getDrawable( R.drawable.shape_circular_progressbar );
        mProgress = view.findViewById( R.id.circularProgressbar );
        mProgress.setProgress( 0 );   // Main Progress
        mProgress.setSecondaryProgress(2); // Secondary Progress
        mProgress.setMax( 100 ); // Maximum Progress
        mProgress.setProgressDrawable( drawable );
        tv = view.findViewById( R.id.tv );
        new Thread( new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                while (pStatus < 100) {
                    pStatus += 1;

                    handler.post( new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            mProgress.setProgress( pStatus );
                            tv.setText( pStatus + "%" );

                        }
                    } );
                    try {
                        // Sleep for 200 milliseconds.
                        // Just to display the progress slowly
                        Thread.sleep( 8 ); //thread will take approx 1.5 seconds to finish
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        } ).start();

    }}
