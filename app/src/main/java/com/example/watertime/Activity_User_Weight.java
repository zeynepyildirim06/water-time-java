package com.example.watertime;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class Activity_User_Weight extends AppCompatActivity {
    com.shawnlin.numberpicker.NumberPicker numberPicker;
    private static final String TAG = null;

    public static View view;
    ImageView imgHigher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_user_weight );

        numberPicker = findViewById( R.id.activity_user_weight_numberPicker );
        imgHigher = findViewById( R.id.activity_user_weight_img_go );

        numberPicker.setMaxValue( 180 );
        numberPicker.setMinValue( 30 );
        numberPicker.setValue( 3 );

        imgHigher.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( getApplicationContext(), Activty_Wake_Up.class );
                startActivity( intent );
            }
        } );
// OnClickListener
        numberPicker.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d( TAG, "Click on current value" );
            }
        } );
    }


}


