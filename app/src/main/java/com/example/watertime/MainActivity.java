package com.example.watertime;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.PopupWindow;
import android.widget.TextView;


import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.xw.repo.BubbleSeekBar;

import java.lang.reflect.Field;

import static androidx.core.content.ContextCompat.getSystemService;

public class MainActivity extends AppCompatActivity {
    public static MainActivity Instance;

    public BubbleSeekBar seekBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_main );

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.bottomNavigationView);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.action_home);
        loadFragment(new Water_Main_Fragment());
        MainActivity.Instance = this;
        Object mContainer;



    }
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment = null;
            switch (item.getItemId()) {
                case R.id.action_home:
                    selectedFragment = new Water_Main_Fragment();
                    loadFragment(selectedFragment);
                    break;
                case R.id.action_settings:
                    selectedFragment = new Settings_Fragment();
                    loadFragment(selectedFragment);
                    break;
                case R.id.action_notification:
                    selectedFragment = new Settings_Fragment();
                    loadFragment(selectedFragment);
                    break;
                case R.id.action_statistic:
                    selectedFragment = new Settings_Fragment();
                    loadFragment(selectedFragment);
                 break;
                case R.id.action_water_add:
                    selectedFragment = new Settings_Fragment();
                    loadFragment(selectedFragment);
                   break;

            }

            return true;
        }
    };

    public void loadFragment(Fragment fragment) { //fragmentlarımızı çağırdığımız fonksiyon

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.mainFrameLayout, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    public void setPopUp(View v)
    {
        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater) getSystemService( LAYOUT_INFLATER_SERVICE );
        View popupView_unit = inflater.inflate( R.layout.layout_unit_popup, null );

        // create the popup window
        int width = 1100;
        int height = 2200;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow( popupView_unit, width, height,focusable );

        popupWindow.showAtLocation( v, Gravity.CENTER, 0, 0 );

        // dismiss the popup window when touched
        popupView_unit.setOnTouchListener( new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        } );
    }
    public void setConsumtionPopPup(View v)
    {
        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater) getSystemService( LAYOUT_INFLATER_SERVICE );
        View popupView_unit = inflater.inflate( R.layout.layout_consumption_target_popup, null );
        seekBar = popupView_unit.findViewById( R.id.consumption_seekbar_popup );
        final TextView txt_ml;
        txt_ml = popupView_unit.findViewById( R.id.layout_consumption_target_txt );
        // create the popup window
        int width = 1100;
        int height = 2200;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow( popupView_unit, width, height,focusable );

        seekBar.setOnProgressChangedListener( new BubbleSeekBar.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {
            txt_ml.setText( progress+"" );

            }

            @Override
            public void getProgressOnActionUp(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {

            }

            @Override
            public void getProgressOnFinally(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {

            }
        } );



        popupWindow.showAtLocation( v, Gravity.CENTER, 0, 0 );

        // dismiss the popup window when touched
        popupView_unit.setOnTouchListener( new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        } );
    }

}

