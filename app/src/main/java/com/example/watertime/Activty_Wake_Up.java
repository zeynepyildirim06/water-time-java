package com.example.watertime;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TimePicker;

import com.michaldrabik.classicmaterialtimepicker.CmtpTimeDialogFragment;

import java.sql.Time;

public class Activty_Wake_Up extends AppCompatActivity {
CmtpTimeDialogFragment timePicker;
ImageView imgHigher;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_wake_up );
        timePicker = CmtpTimeDialogFragment.newInstance();

imgHigher=findViewById( R.id.activity_wake_up_img_go);
imgHigher.setOnClickListener( new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent intent = new Intent( getApplicationContext(), Activty_slepping_time.class );
        startActivity( intent );
    }
} );


    }
}
